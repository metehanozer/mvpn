# mvpn
Ubuntu için OpenConnect uygulamasının kullanımını kolaylaştırır.

### Önkoşullar
mvpn aşağıdaki komut ile path'e eklenip heryerden çalıştırılabilir.

~~~
sudo cp mvpn /usr/local/bin
~~~

### Çalıştırma
mvpn scripti aşağıdaki komutlar ile kullanılabilir.

~~~
mvpn --help
mvpn --open
mvpn --close
~~~